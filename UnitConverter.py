import sys

def MilesToKilometers():
    millas = float(input("Entre las millas -> "))
    km = millas * 1.60935
    print("{} millas es equivlente a {} kilómetros.\n".format(millas, km))


def FahrenheitToCelsius():
    temp = float(input("Entre los grados fahrenheit -> "))
    Celsius = (5/9) * (temp -32)
    print("{0} grados Fahrenheit es esquivalente a {1:.2f} grados Celsius.\n".format(temp, Celsius))


def FeetToMeters():
    feet = float(input("Entre los pies -> "))
    meters = feet * 0.3048
    print("{} pies es esquivalente a {} metros.\n".format(feet, meters))

def showMenu():
    print("Las alternativas disponibles son: ")
    print("         1. Convertir de millas a kilómetros.")
    print("         2. Convertir de pies a metros.")
    print("         3. Convertir de grados Fahrenheit a Celsius.")
    print("         4. Terminar.\n")

def menu():
    showMenu()
    choice = float(input("Entre una alternativa (0 para ver el menu). --> \n"))
    if choice < 0 or choice > 4:
        print("opcion incorrecta\n")
        menu()
    elif choice == 0:
        menu()
    elif choice == 1:
        MilesToKilometers()
        menu()
    elif choice == 2:
        FeetToMeters()
        menu()
    elif choice == 3:
        FahrenheitToCelsius()
        menu()
    else:
        print("proceso terminado")
        sys.exit()


menu()